---
title: Christophylax
date: 2024-02-05
aside: false
pageClass: "center"
---
<br>

*Into a cave deep\
We did wander\
To find a dragon and keep*

*A mighty serpent\
Less than certain\
Behind the curtain\
A mere sheep*
<br>
<br>
<br>
*Christophylax\
Ears full of wax\
Did not hear what we spake*

*Fearful wonder\
Will he plunder\
Tear asunder\
Flesh from bone shall he rake*
<br>
<br>
<br>
*Now he's somber\
Clouds of thunder\
To the truth he must wake*

*Evil forces\
Sewed confusion\
A cruel Illusion\
The egg was a fake*
<br>
<br>
<br>
*Christophylax\
Needs to relax\
Turn that frown upside down*

*With your mind back\
Make a swift act\
Clear the fog\
From the town.*