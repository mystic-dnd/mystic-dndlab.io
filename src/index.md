---
layout: home
aside: false
hero:
  name: Mystic D&D
  text: ''
  actions:
    - text: Whyatt's journal
      link: /journal/
    - text: Bernie's Song book
      link: /song-book/
    - text: Dice
      link: /dice
---