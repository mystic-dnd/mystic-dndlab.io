---
title: The warehouse
date: 2022-10-03
aside: false
excerpt: Staking out the Baroness' warehouse we quickly find ourselves in danger.
---
To uncover the mystery of the Barron’s business troubles our next pursuit was to predict and counter
any ambush on the wagon that made delivery to the Barron’s warehouse.

Arriving at the warehouse we surveyed the area. Daro attempted and failed to climb a wall. No one
noticed.

The plan we formed was to find hiding places and lay in wait for an attack. Our first mistake was to put
too much space between us. Our second was to break line of sight.

Sapphire, Hildrid and Daro each found a dark alleyway. I climbed a ladder to the warehouse roof to find
Nox already there – he must have found another ladder.

Then the delivery arrived. Unloaded by halflings for some reason.
Suddenly we were surrounded. By my count there ware ten assailants. Though some of the party recall
more than fifty.

The first of us to notice was Daro. A kindred thief tapped the Tabaxi on the shoulder. Her first thought
self preservation, cast out a bag of ball-bearings; which along with Nox’s invisible helper proved an
effective obstacle to advances from the ally. 
Throughout the fight Daro used her agility to dodge and parry several attacks, but still suffered multiple
hits. Surrounded by the enemy Daro was saved from life threatening injuries by a well timed
intervention from Nox.

Nox found success with well placed knife throws and Endrich incantations of black and white. In the
fight he took at least three crossbow bolts, I’m surprised he was still standing. The effectiveness of the
colourful clothes I have yet to determine.

Sapphire fought a fierce solo battle fending off several attackers, making use of her surroundings for
shelter. She also sustained wounds from multiple crossbow bolts. She appears to wield magic that can
play tricks on the mind, here sending the enemy running in fear.
A fearful thought. Something to bear in mind, I already hear her voice in mine.

Hildrid fought a ferocious battle of his own. Contending between three and thirty soldiers, depending
who you ask. A battle of great martial skill all the same. And continued until the job was done, clearing
the street of bodies and binding the surviving thief.
We found a mixture of Rags and mercenaries amongst them.

The long bow felt good in my hands. However the words ‘look before you leap’ weighed heaver. At least
we all survived, a grace in any combat. But again, we were surprised by the enemy.

Most now dead, including one of the halflings the assault dissipated. One captured thief
lay unconscious when the city guard arrived.
Unable to convince them to give us custody of the thief we agreed to accompany them back to their
barracks to witness the interrogation.

It was then that Daro decided to pick a fight with the Barron’s guard captain, who moments ago fought
by our side. With the rest of us watching on I think the feeling was mutual that a night in a cell was the
best outcome.

Joining the city guards at their barracks Nox managed to convince the guards that Sapphire should
conduct the interrogation.
Presented with torture tools that would turn the stomach of any hardened warrior, Sapphire instead
used mind altering magic to great effect; persuading the thief to give up their employer.
