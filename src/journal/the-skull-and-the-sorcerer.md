---
title: The Skull and the Sorcerer
date: 2022-11-14
aside: false
excerpt: An ancient underground ruin holds secrets and fiery death.
---
A fireball flies past. It bursts, engulfing the stone walls in green flame. My party are quicker than me, they stave the worst. I go down, down into darkness.

I'm getting ahead of myself.

En route to Tenacity mine we made camp off of the main road. In the cover of the woods a small creature approaches; a kobold named Qwirkle. He is injured and desperate for help. His nest he recounts was attacked by a so called *fiery death*.\
Daro is evidently on edge. The threat of this *Leatherset* on all our minds.

Qwirkle led four of us to their nest and to the ruins the kobolds have uncovered with their mining. In the underground ruins we find the perpetrator of this fiery death; a floating skull ablaze with green flames patrolling the ancient corridors.

Nox hatched a plan to disarm the entity without combat. To convince the skull to stand down, disguised and unguarded was certainly brave. Unfortunately the deception fall short and is undone.

We set upon the skull with martial and magical combat. Not every attack lands true, but we are faring well.\
Something new. A fireball, much larger than the others. It lights up the forgotten walls fit for a tomb, the flames green and violent. My party are quicker than me, they escape the worst. The skull is laughing. I go down, searing pain no more, only darkness.

No, not just darkness. There were forms. Were they faces? I don't remember them. Falling deeper.
Then a figure before me, winged and shining. My wælcyrġe, takes hold of me in the dark. Sapphire, the one who talks in my mind. Pulls me forward, not to the next life, but back to the living.

![Sapphire](./sapphire.png)

The skull finally destroyed we rest bloodied, patching our wounds, deciding whether to explore deeper.
Qwirkle convinced by Nox, now believes himself to be a great sorcerer.\
But our rest is cut short. Green flames light the corridors. The floating skull has somehow returned, laughs and taunts once again.

As we scrambled to prepare for another fiery encounter, the first cast is made, a flame strikes the small kobold turning them in an instant to ash. For our little sorcerer, a fiery death indeed.

Strong strikes from Hildred, Nox and everything I could still muster. Blow after blow we put the skull down once more. Having leaned more of it's nature, Hildred used holy water to put an end to it's rejuvenation.

I now understand that these *flameskulls* are created by spellcasters to serve as guards. Judging by the age of the ruin it's creator is most likely long gone, but I fear in exploring whatever it was guarding, great caution must be taken.
