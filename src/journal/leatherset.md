---
title: Leatherset
date: 2023-01-16
aside: false
excerpt: 'The hunter becomes the hunted'
---
We had learned that Leatherset could be close by. The party decided to take the initiative and investigate their whereabouts.

Having identified our hunter we set a trap. Or sprung a trap.\
Note to self: Check for traps.

This Leatherface had come prepared and dealt some crewel strikes. Fortunately for us not so strong in an open fight, where we were able to overpower him. 

As we fought Leatherchest, Calannis was badly wounded; though thankfully will recover. Adventuring on my own I have learned to use my speed. I must remember my party, this is our strength. And perhaps find a way to better draw the attention of would be dangers.

>I did not become someone different\
>That I did not want to be\
>But I'm new here\
>Will you show me around
>
>No matter how far wrong you've gone\
>You can always turn around

Now bound, Leathersack was an odd character. An assassin of little moral substance. He did reveal the benefactor of the bounty on us. Another thread to follow.

When the party left the hill two stayed behind. Nox was talking loudly, but that's not unusual. Auspiciously an act of compassion, I don't know what happened between Calannis and Daro and Leatherwaist. Waste of leather.

I know that adventuring does take one to strange new places and meet extraordinary people. I wasn't prepared however for quite so many whiskers. They are just illusions I tell myself. No matter how I look, I'm not really changing. I wonder what you look like.
