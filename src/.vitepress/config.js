export default {
    title: 'Mystic D&D',
    description: ' ',
    appearance: false,
    themeConfig: {
        nav: [
            { text: 'Whyatt\'s Journal', link: '/journal/' },
            { text: 'Bernie\'s Song book', link: '/song-book/' },
            { text: 'Dice', link: '/dice' }
        ],
        footer: {
            message: `
            <a href="https://gitlab.com/mystic-dnd/mystic-dnd.gitlab.io" target="__blank" rel="noopener">Source code</a><br/>
            Art by <a href="https://www.wyliebeckert.com/">www.wyliebeckert.com</a>
            `,
        }
    },
    markdown: {
        anchor: {
            level: 0,
            tabIndex: false
        }
    }
}