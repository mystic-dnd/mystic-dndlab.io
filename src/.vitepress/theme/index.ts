import DefaultTheme from 'vitepress/theme';
import BlogIndex from './BlogIndex.vue';
import BlogIndexJournal from './BlogIndexJournal.vue';
import BlogIndexSongbook from './BlogIndexSongbook.vue';
import Dice from './Dice.vue';
import Layout from './Layout.vue';

import './font.css';
import './style.css';

export default {
    ...DefaultTheme,
    Layout,
    enhanceApp({ app }) {
        app.component('BlogIndex', BlogIndex);
        app.component('BlogIndexJournal', BlogIndexJournal);
        app.component('BlogIndexSongbook', BlogIndexSongbook);
        app.component('Dice', Dice);
    }
}